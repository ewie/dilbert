#!/bin/sh

DT=86400
UT=$(date '+%s')
EXISTS=0

while true; do
  DATE=$(date -d "@$UT" '+%Y-%m-%d')
  DIR=$(date -d "@$UT" '+%Y/%m')
  FNAME="$DIR/$DATE"
  if [[ ! -f "$FNAME.gif" ]] && [[ ! -f "$FNAME.jpg" ]]; then
    # The meta tags of the head section may contain incorrect URLs. The image tag has the correct URL and is the last occurence.
    URL=$(curl -s "http://dilbert.com/strip/$DATE" | grep -o 'http://assets.amuniversal.com/[0-9a-f]\+' | tail -n 1)
    mkdir -p "$DIR"
    curl -s -L -o "$FNAME" "$URL"
    if [[ -f "$FNAME" ]]; then
      if [[ $EXISTS -ne 0 ]]; then
        EXISTS=0
        echo
      fi
      echo $DATE $URL
      echo $DATE $URL >> index
      if [[ $(head -c 6 "$FNAME") = "GIF89a" ]]; then
        EXT="gif"
      else
        EXT="jpg"
      fi
      mv "$FNAME" "$FNAME.$EXT"
    fi
    sleep 1
  else
    EXISTS=1
    echo -en "\e[0K\r$DATE"
  fi
  UT=$(echo "$UT - $DT" | bc)
done

